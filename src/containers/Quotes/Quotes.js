import React, {Component, Fragment} from 'react';
import './Quotes.css';
import axios from "../../axios-blog";
class Quotes extends Component {
    state = {
      quotes: null
    };

    componentDidMount() {
        axios.get('/quotes.json').then(response => {
            if(!response.data) return null;
                const quotes = Object.keys(response.data).map(id => {
                return {...response.data[id], id};
                });
            this.setState({quotes: quotes})
        });

    }
    deleteHandler = () => {
        axios.delete(this.props.match.url + '.json').then(() => {
            this.props.history.push('/')
        })
    };
    render() {
        let quotes = null ;
        if(this.state.quotes )  {
            quotes = this.state.quotes.map((quote, id) => (
                <div key={id} className="post-title"><h3 className="text-title">Category : {quote.title}</h3><h4 className='text-title'>Author : {quote.author}</h4><p className="description">Quote : {' " '} {quote.description} {' " '}</p><button onClick={this.deleteHandler} className="btn-link">View</button></div>
            ))
        }
        return (
         <Fragment>
             {quotes}
         </Fragment>
        );
    }
}

export default Quotes;