import React, {Component, Fragment} from 'react';
import './container.css';

import {BrowserRouter as Router, Route} from "react-router-dom";
import Header from "../components/Header/Header";
import Quotes from "./Quotes/Quotes";
import AddQuotePost from "./AddNewQuote/AddQuotePost";



class Container extends Component {

    render() {
        return (
            <div className="container">
                <Router>
                    <Fragment>
                        <Header/>
                        <Route path="/all"  exact component={Quotes}/>
                        <Route path="/submit" component={AddQuotePost}/>
                        <Route path='/starWars' component={Quotes}/>
                    </Fragment>
                </Router>
            </div>
        );
    }
}

export default Container;