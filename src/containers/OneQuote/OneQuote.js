import React, {Component} from 'react';
import axios from '../../axios-blog';
import './OneQuote.css'
import {NavLink} from "react-router-dom";
class OneQuote extends Component {
    state = {
      oneQuote: null
    };

    componentDidMount() {
        axios.get(this.props.match.url + '.json')
        .then(response => {
         this.setState({oneQuote: response.data});
      });
    }

    deleteHandler = () => {
        axios.delete(this.props.match.url + '.json').then(() => {
            this.props.history.replace('/')
        })
    };
    render() {
        let oneQuote = null;
        if (this.state.oneQuote) {
            return <div className="onePost"><h3 className="text-title">{this.state.oneQuote.title}</h3>
                <h4 className='text-title'>{this.state.oneQuote.author}</h4>
                <p className="description-text-area">{this.state.oneQuote.description}</p>
                <button className="delete" onClick={this.deleteHandler}>Delete</button>
                <NavLink to= {"/post/" + this.props.match.params.id + '/edit'}><button className="edit">Edit</button></NavLink></div>
        }
        return (
            <div>
                {oneQuote}
            </div>
        );
    }
}

export default OneQuote;