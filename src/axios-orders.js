import  axios from  'axios'

const instance = axios.create({
    baseURL: 'https://quates-central.firebaseio.com/'
});

export default instance;